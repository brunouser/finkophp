<?php


class PenaltiesGenerator
{

    /**
     * Generates list of loan penalties
     *
     * Penalty must be generated for each date at which loan is considered delayed
     * Penalty amount is $rate percent of loan principal amount, rounded till nearest integer with default mode (PHP_ROUND_HALF_UP)
     *
     * List must be sorted by date ascending
     *
     * @param Loan $loan
     * @param int $rate Percent value
     * @return array|Penalty[]
     */

    public function arrayRange(array $array) : array
    {
         $returnValue = [];

         foreach ($array as $data) {
             $start = new Date($data->start->format(DATE_TYPE));
             $end = new Date($data->end->format(DATE_TYPE));
             $range = new DatePeriod($start, new DateInterval('P1D'), $end);

             foreach ($range as $key => $value) {
                 array_push($returnValue, $value->format(DATE_TYPE));
             }
         }

         return $returnValue;
    }

    function dateSort(Penalty $a, Penalty $b) : int
    {
        return strtotime($a->date->format(DATE_TYPE)) <=> strtotime($b->date->format(DATE_TYPE));
    }

    public function generate(Loan $loan, int $rate) : array
    {
        $delays = $this->arrayRange($loan->delayPeriods);
        $graceDays = $this->arrayRange($loan->gracePeriods);
        $returnValue = [];
        $penaltySum = ($loan->principal * $rate) / 100;
        $penaltySum = round($penaltySum, 0, PHP_ROUND_HALF_UP);
        $newDelays = array_diff($delays, $graceDays);

        foreach ($newDelays as $value) {
            array_push($returnValue, new Penalty(new Date($value), $penaltySum));
        }
        usort($returnValue, [$this, 'dateSort']);

        return $returnValue;
    }
}
