<?php


class Loan
{
    /**
     * Principal amount
     * @var int
     */
    public $principal;

    /**
     * Periods in which loan is considered delayed.
     * Not sorted
     * Not overlapping
     * @var Period[]
     */
    public $delayPeriods = [];

    /**
     * Periods in which loan is considered NOT delayed.
     * Not sorted
     * Not overlapping
     * @var Period[]
     */
    public $gracePeriods = [];

    public function is_in_array(array $array, Date $date) : bool
    {
        foreach ($array as $value) {
            foreach ($value as $arrayDate) {
                return $date->format(DATE_TYPE) === $arrayDate->format(DATE_TYPE);
            }
        }
        return false;
    }

    function getMaxValue(array $array) : string
    {
        $max = '';
        foreach ($array as $value) {
            if ($value->end->format(DATE_TYPE) > $max) {
                $max = $value->end->format(DATE_TYPE);
            }
        }
        return $max;
    }

    /**
     * Returns true if loan is considered delayed at $date.
     * Date is considered delayed if it falls in any of $delayPeriods, but not in $gracePeriods
     * @param Date $date
     * @return bool
     */
    public function isDelayedAt(Date $date) : bool
    {
        return $this->is_in_array($this->delayPeriods, $date) && !$this->is_in_array($this->gracePeriods, $date);
    }

    /**
     * Max count of consecutive dates when loan was delayed without interruption
     * @return int
     */

    public function getMaxUninterruptedDelayDays() : int
    {
        $start = new Date($this->getMaxValue($this->delayPeriods));
        $end = new Date($this->getMaxValue($this->gracePeriods));
        return $start->diff($end)->d;
    }
}
