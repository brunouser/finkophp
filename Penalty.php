<?php


class Penalty
{
    /**
     * @var Date
     */
    public $date;

    /**
     * @var int
     */
    public $amount;

    /**
     * @param Date $date
     * @param int $amount
     */
    public function __construct(Date $date, int $amount)
    {
        $this->date = $date;
        $this->amount = $amount;
    }
}
