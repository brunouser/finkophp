<?php

/**
 * Date period
 * Start is included in interval
 * End is not included in interval
 * Start is less or equal to end
 */
class Period
{
    /**
     * @var Date
     */
    public $start;

    /**
     * @var Date
     */
    public $end;

    /**
     * @param Date $start
     * @param Date $end
     */
    public function __construct(Date $start, Date $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * Days in period
     * @return int
     */
    public function getDays() : int
    {
        $start = new Date($this->start->format(DATE_TYPE));
        $end = new Date($this->end->format(DATE_TYPE));
        $diff = $start->diff($end)->d;
        return $diff;
    }
}
