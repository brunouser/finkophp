<?php


class Date extends \DateTime
{
    public static function d(int $d, int $m, int $y)
    {
        $dt = new Date();
        $dt->setDate($y, $m, $d);
        $dt->setTime(0, 0, 0);
        return $dt;
    }
}
