<?php

/**
 * 1. Implement method Period::getDays
 * 2. Implement method Loan::isDelayedAt
 * 3. Implement method Loan::getMaxUninterruptedDelayDays
 * 4. Implement method PenaltiesGenerator::generate
 *
 * 5. Implement all code for simple schedule generation.
 * Schedule is principal amount that is distributed over number of installments and interest rate is applied on principal amount.
 * Add 3 significant test cases for schedule generator.
 *
 * Schedule example:
 * Principal: 75
 * Installments: 4
 * Interest rate: 17%
 *
 * Schedule:
 * 1. principal: 18, interest: 3
 * 2. principal: 18, interest: 3
 * 3. principal: 18, interest: 3
 * 4. principal: 21, interest: 4
 *
 * GENERAL RULES:
 * 1. Don't include external libraries
 * 2. PHP version 7.2
 * 3. Feel free to add new classes and new methods to existing classes
 * 4. Specific details for task requirements and clues can found in code comments and test cases,
 *    however if tasks are not clear enough fee free to contact us
 */

foreach (glob(__DIR__ . "/*") as $path) {
    if (preg_match('/\.php$/', $path) && basename($path) !== basename(__FILE__)) {
        require_once $path;
    }
}

$dp1 = new Period(Date::d(1, 1, 2019), Date::d(5, 1, 2019));
assert($dp1->getDays() === 4); // 2

$dp2 = new Period(Date::d(10, 1, 2019), Date::d(20, 1, 2019));
assert($dp2->getDays() === 10); // 5

$dp3 = new Period(Date::d(23, 1, 2019), Date::d(20, 2, 2019));
assert($dp3->getDays() === 28); // 6

$gp1 = new Period(Date::d(3, 1, 2019), Date::d(6, 1, 2019));
assert($gp1->getDays() === 3);

$gp2 = new Period(Date::d(15, 1, 2019), Date::d(14, 2, 2019));
assert($gp2->getDays() === 30);

$loan = new Loan();
$loan->principal = 135;
$loan->delayPeriods[] = $dp1;
$loan->delayPeriods[] = $dp2;
$loan->delayPeriods[] = $dp3;
$loan->gracePeriods[] = $gp1;
$loan->gracePeriods[] = $gp2;

assert($loan->isDelayedAt(Date::d(26, 12, 2018)) === false);
assert($loan->isDelayedAt(Date::d(1, 1, 2019)) === true);
assert($loan->isDelayedAt(Date::d(4, 1, 2019)) === false);

assert($loan->getMaxUninterruptedDelayDays() === 6); //14.02 - 20.03

$pg = new PenaltiesGenerator();
$penalties = $pg->generate($loan, 19);

assert(count($penalties) === 13);
assert($penalties[0]->date->format(DATE_TYPE) === '01.01.2019');
assert($penalties[0]->amount === 26);

assert($penalties[12]->date->format(DATE_TYPE) === '19.02.2019');
assert($penalties[12]->amount === 26);

$penalties2 = $pg->generate($loan, 18);
assert($penalties2[0]->amount === 24);

//more test cases for getMaxUninterruptedDelayDays()
$loan2 = new Loan();
$loan2->principal = 135;
$loan2->delayPeriods[] = new Period(Date::d(1, 1, 2019), Date::d(10, 1, 2019));
$loan2->gracePeriods[] = new Period(Date::d(3, 1, 2019), Date::d(6, 1, 2019));
assert($loan2->getMaxUninterruptedDelayDays() === 4); //06.01 - 10.01

//schedule generator test cases goes here
$scheduleGenerator = new ScheduleGenerator(75, 4, 17);
$sg = $scheduleGenerator->init();
assert($sg[1]->payment === 18);
assert($sg[3]->interest === 4);
assert($sg[2]->interest === 3);

die('End.');
