<?php

class ScheduleGenerator
{
    public function __construct(int $principal, int $installments, int $interestRate)
    {
        $this->principal = $principal;
        $this->installmentsCount = $installments;
        $this->interestRate = $interestRate;
    }

    public $lastInstallment = 0; // 21
    public $regularInstallment = 0; // 18
    public $regularInterest = 0; // 3
    public $lastInterest = 0; // 4


    public function setupValues() : void
    {
        $installmentAmmount = $this->principal / $this->installmentsCount;
        $this->regularInstallment = floor($installmentAmmount);
        $installmentFraction = round(($installmentAmmount - $this->regularInstallment) * ($this->installmentsCount - 1), 2);
        $this->lastInstallment = ceil($installmentAmmount + $installmentFraction);
        $allInterest = ceil(($this->principal * $this->interestRate) / 100);
        $interestFraction = ($allInterest / $this->installmentsCount - floor($allInterest / $this->installmentsCount)) * ($this->installmentsCount - 1);
        $this->regularInterest = floor($allInterest / $this->installmentsCount);
        $this->lastInterest = ceil($this->regularInterest + $interestFraction);
    }

    public function generateInstallmentList() : array
    {
        $data = [];
        for ($i = 1; $i < $this->installmentsCount; $i++) {
            array_push($data, new SchedulePayment($this->regularInstallment, $this->regularInterest));
        }
        array_push($data, new SchedulePayment($this->lastInstallment, $this->lastInterest));
        return $data;
    }

    public function init() : array
    {
        $this->setupValues();
        return $this->generateInstallmentList();
    }
}
